import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../iterfaces/student';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  @Input() math:number;
  @Input() psyc:number;
  @Input() pay:boolean;
  @Input() id:string;
  @Input() result:string;
  @Output() update = new EventEmitter<Student>();
  @Output() predict = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
  

  errorMessageMath:string;
  isErrorMath:boolean =false;
  errorMessagePsyc:string;
  isErrorPsyc:boolean =false;
  dropDwonState:boolean = false;
  dropDownShow = false;
  options:Object[] = [{id:1,name:true},{id:2,name:false}]

  updateParent(){
    let student:Student = {id:this.id, math:this.math, psyc:this.psyc, pay:this.pay,result:this.result};
    if(this.math< 0 || this.math>100){
      this.isErrorMath=true;
      this.errorMessageMath = "Please inseret number in rang 0,100";}
      if(this.psyc< 0 || this.psyc>800){
        this.isErrorPsyc=true;
        this.errorMessagePsyc = "Please inseret number in rang 0,800";
    }else{
      this.update.emit(student); 
    }
    // if(this.formType == "Add Student"){
    //   this.math  = null;
    //   this.psyc = null;
    //   this.pay = null; 
    // }

  }
  tellParentToPredic(){
    let student:Student = {id:this.id, math:this.math, psyc:this.psyc, pay:this.pay};
    this.predict.emit(student); 
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  onSubmit(){
    this.router.navigate(['/table']);
  }

  constructor( private router:Router) { }

  ngOnInit(): void {
  }

}
