import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://bzgv5zc8h3.execute-api.us-east-1.amazonaws.com/beta";


  predict(math:number, psyc:number, pay:number):Observable<any>{
    let json = {
      "data": 
        {
          "math": math,
          "psyc": psyc,
          "pay": pay
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor( private http:HttpClient) { }
}
