import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../iterfaces/student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-stable',
  templateUrl: './stable.component.html',
  styleUrls: ['./stable.component.css']
})
export class StableComponent implements OnInit {

  displayedColumns: string[] = ['math', 'psyc', 'pay','result','UserEmail','delete'];

userId:string;
email:string;
students$
studnets:Student [];

delete(id:string){
  this.studentsSrevice.deleteStudent(this.userId,id);
}
  constructor(private studentsSrevice:StudentsService, public authService:AuthService) { }

  ngOnInit(): void {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
        this.students$ = this.studentsSrevice.getStudents(this.userId);
        console.log(this.userId)
        this.students$.subscribe(
          docs => {
           this.studnets = [];
            for(let document of docs){
              const studet:Student = document.payload.doc.data();
              // if(studet.result){
              //   studet.saved = true; 
              // }
              studet.id = document.payload.doc.id;
              this.studnets.push(studet);
            }
          }
       )
      }
    )
  }



  

}
