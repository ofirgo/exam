export interface Student {
    id:string,
    name?:string;
    email?:string,
    math: number,
    psyc: number,
    pay:boolean,
    result?:string,
    saved?:boolean

}
