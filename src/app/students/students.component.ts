import { PredictService } from './../predict.service';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../iterfaces/student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

userId:string;
students$
studets:Student [];
pay;
result:string;



  save(studet:Student){
    this.studentsSrevice.addStudent(this.userId,studet.math,studet.psyc,studet.pay,studet.result); 
  }

  preidct(studet:Student){
    if (studet.pay){
      this.pay = 0
    }
    if (!studet.pay){
      this.pay =1
    }
    this.PredictService.predict(studet.math,studet.psyc,this.pay).subscribe(
      res=>{
          console.log(res);
        if(res > 0.5){
         this.result = "Will not leave";
        }else{
          this.result = "Will leave";
        }
        studet.result = this.result;
      }
    )
  }

  constructor(private studentsSrevice:StudentsService, public authService:AuthService, private PredictService:PredictService) { }

  ngOnInit(): void {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.studentsSrevice.getStudents(this.userId);
        console.log(this.userId)
        this.students$.subscribe(
          docs => {
           this.studets = [];
            for(let document of docs){
              const studet:Student = document.payload.doc.data();
              // if(studet.result){
              //   studet.saved = true; 
              // }
              studet.id = document.payload.doc.id;
              this.studets.push(studet);
            }
          }
       )
      }
    )
  }

}
