import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  userCollection:AngularFirestoreCollection= this.db.collection('users');
  studentsCollection:AngularFirestoreCollection ;

  public getStudents(userId){
    this.studentsCollection = this.db.collection(`users/${userId}/students`);
    return this.studentsCollection.snapshotChanges();
  }

  addStudent(userId:string,math:number,psyc:number,pay:boolean,result:string){
    const student = {math:math, psyc:psyc,pay:pay,result:result}; 
    this.userCollection.doc(userId).collection('students').add(student);
  }

  public deleteStudent(userId:string , id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
  }
  constructor(private db:AngularFirestore) { }
}
